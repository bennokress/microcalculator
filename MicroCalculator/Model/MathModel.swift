//
//  MathModel.swift
//  MicroCalculator
//
//  Created by Benno Kress on 23.10.17.
//  Copyright © 2017 Benno Kress. All rights reserved.
//

import Foundation
import SwifterSwift

/// This struct contains methods and variables that are not visible to the Objective C app, but are used by functions marked as public and @objc ...
struct MathModel {
    
    /// Adding the neutralNumber to any number in the original number ... it's magic :)
    static var neutralNumber: Int = 0
    
    /// A function that demonstrates the usage of a dependency of this framework
    static func romanNumeral(for integer: Int) -> String {
        return integer.romanNumeral() ?? "ERROR"
    }
    
    /// Multiplies the two numbers
    static func multiply(_ int1: Int, with int2: Int) -> Int {
        return int1 * int2
    }
    
    /// Divides the two numbers and rounds the result
    static func divide(_ int1: Int, by int2: Int) -> Int {
        return int1 / int2
    }
    
}
