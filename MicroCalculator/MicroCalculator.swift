//
//  MicroCalculator.swift
//  MicroCalculator
//
//  Created by Benno Kress on 16.10.17.
//  Copyright © 2017 Benno Kress. All rights reserved.
//

import Foundation

/// This class contains all the accessible methods. To use it from Objective C every property and function has to be marked with @objc.
public class MicroCalculator: NSObject {
    
    /// Calculates the cooking time in seconds for usable watt number of a microwave with the data provided by the food manufacturer.
    @objc public func getAdjustedCookingTime(for usedWattNumber: Int, originalWattNumber: Int, originalCookingTime: Int) -> Int {
        if usedWattNumber == 0 {
            print("PSA from the framework: Romans don't know numbers below 1, sad!")
            return MathModel.neutralNumber
        } else {
            let specifiedProduct = MathModel.multiply(originalWattNumber, with: originalCookingTime)
            let adjustedCookingTime = MathModel.divide(specifiedProduct, by: usedWattNumber)
            print("PSA from the framework: Romans would microwave the food for \(MathModel.romanNumeral(for: adjustedCookingTime)) seconds!")
            return adjustedCookingTime
        }
    }
    
}
